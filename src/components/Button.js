import React from 'react';
import {View, Image, Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import RF from "react-native-responsive-fontsize";
import ResponsiveText from "./ResponsiveText";
import Fonts from "../config/styles/Fonts";
import Color from "../config/styles/Color";

class Button extends React.Component {
    render() {
        // if (this.props.leftIcon != null)
        //     return (
        //         <TouchableOpacity onPress={this.props.onPress}>
        //             <View style={{...styles.container, ...this.props.containerStyle}}>
        //                 <Image style={{width: RF(4), height: RF(4), marginRight: RF(2)}}
        //                        source={this.props.leftIcon}/>
        //                 <ResponsiveText style={this.props.btnText}>{this.props.text}</ResponsiveText>
        //             </View>
        //         </TouchableOpacity>
        //     );
        // else
            return (
                <TouchableOpacity onPress={this.props.onPress} style={this.props.containerStyle}>
                    <View style={{...styles.container, ...this.props.contentStyle}}>
                        {
                            this.props.leftIcon &&
                            <Image style={{width: RF(4), height: RF(4), marginRight: RF(2)}}
                                   source={this.props.leftIcon}/>
                        }
                        {
                            this.props.loading && <ActivityIndicator size="small" color={Color.Placeholder}/> ||
                            <ResponsiveText style={this.props.btnText}>{this.props.text}</ResponsiveText>

                        }
                    </View>
                </TouchableOpacity>
            );
    }

}

export default Button;
const styles = {
    container: {
        width: '80%',
        // height: RF(8),
        paddingVertical:12,
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: "grey"
    }
}
