import React from 'react';
import {View, Text} from 'react-native';
import CustomButton from "./CustomButton";
import RF from "react-native-responsive-fontsize";
import ResponsiveText from "./ResponsiveText";
import Fonts from "../config/styles/Fonts";

class WelcomeFooter extends React.Component {
    render() {
        return (
            <View style={style.container}>
                <ResponsiveText style={style.text}>Registrate o inicia sesion con tu cuenta {"\n"} para acceder.</ResponsiveText>
                <View style={style.buttonView}>
                    <CustomButton text={'Iniciar'}
                    onPress={this.props.LogIn}
                    />
                    <CustomButton text={'Registrar'}
                    onPress={this.props.Registrar}
                    />
                </View>
            </View>
        );
    }
}
const style = {
    container:{
       /* backgroundColor:'silver',*/
        flex:1,
        width:'100%',
        paddingHorizontal:RF(3)
    },
    text: {
        color: 'white',
        textAlign: 'center',
        fontSize:Fonts.h3,
    },
    buttonView:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:RF(3),
        /*backgroundColor:'black'*/
    }
}
export default WelcomeFooter;