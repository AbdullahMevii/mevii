import React from 'react';
import {View,TouchableOpacity,Image} from 'react-native';
import RF from "react-native-responsive-fontsize";
import Color from "../config/styles/Color";

class CheckBox extends React.Component
{
    render()
    {
        return(
            <TouchableOpacity
                onPress={this.props.onPress}>
                <View style={{marginTop:RF(0.5),marginRight:RF(1),width:RF(2.5),height:RF(2.5),borderColor:'grey',backgroundColor:Color.Placeholder,
                    justifyContent: 'center',alignItems: 'center',borderRadius:RF(.5),borderWidth: RF(.2)

                }}>
                    {
                        this.props.value &&

                        <View style={{width:RF(2.5),height:RF(2.5),borderColor:'grey',backgroundColor:Color.Placeholder,
                            justifyContent: 'center',alignItems: 'center',borderRadius:RF(.5),borderWidth: RF(.2)

                        }}>
                            <Image source={require('../../assets/tick.png')}
                                   style={{width:RF(1.4),height:RF(1.4),resizeMode:'contain',tintColor:'white'}}
                            />
                        </View>
                    }
                </View>
            </TouchableOpacity>
        );
    }

}
export default CheckBox;