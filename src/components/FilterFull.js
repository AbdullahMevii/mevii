import React from 'react';
import {View, TouchableOpacity,SafeAreaView} from "react-native";
import ResponsiveText from "./ResponsiveText";
import RF from "react-native-responsive-fontsize";
import Fonts from "../config/styles/Fonts";
import Color from "../config/styles/Color";

class FilterFull extends React.Component {
  state = {leftButton: false, rightButton: true}

  render() {
    return (
      <SafeAreaView stye={{backgroundColor: 'white',flex:1}}>
        <View style={{marginTop: RF(1), paddingLeft: RF(2), paddingTop: RF(1)}}>
          <ResponsiveText style={{
            marginBottom: RF(2),
            fontWeight: 'bold',
            fontSize: Fonts.h1,
            color: Color.Placeholder
          }}>Filtro</ResponsiveText>
          <ResponsiveText style={{color: Color.Placeholder, fontSize: Fonts.h3}}>Buscando propiedades</ResponsiveText>
        </View>

        <View style={styles.buttonContainer}>

          <TouchableOpacity style={{flex: 1}} onPress={() => this.setState({leftButton: true, rightButton: false})}>
            <View style={[styles.leftButton,this.state.leftButton&&{backgroundColor:'#5184C7'}]}>
              <ResponsiveText style={[styles.text,this.state.leftButton &&{color:'white'}]}>En alquier</ResponsiveText>
            </View>
          </TouchableOpacity>


          <TouchableOpacity style={{flex: 1}} onPress={() => this.setState({rightButton:true ,leftButton:false })}>
            <View style={[styles.rightButton,this.state.rightButton && {backgroundColor: "#5184C7"}]}>
              <ResponsiveText style={[styles.text,this.state.rightButton && {color:'white'}]}>En venta</ResponsiveText>
            </View>
          </TouchableOpacity>
        </View>

      </SafeAreaView>
    );
  }
}

export default FilterFull;
const styles = {
  buttonContainer: {
    height: RF(7),
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#5184C7',
    width: '90%',
    alignSelf: 'center',
    borderRadius: RF(1),
    marginTop: RF(2)
  },
  leftButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 1,
    borderRightColor: '#5184C7'
  },
  leftButtonActive:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 1,
    borderRightColor: '#5184C7',
    backgroundColor: '#5184C7'
  }
  ,
  rightButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
rightButtonActive:{
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#5184C7'
},
  text:{
    color:'#5184C7'
  }
}