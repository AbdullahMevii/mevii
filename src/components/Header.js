import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import RF from "react-native-responsive-fontsize";

class Header extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={{paddingTop: RF(2), paddingLeft: RF(2)}}>
          <Image style={{height: RF(5), width: RF(5)}} source={require('../../assets/back.png')}/>
        </View>
      </TouchableOpacity>
    );
  }
}

export default Header;