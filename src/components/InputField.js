import React from "react";
import {StyleSheet, TouchableOpacity, TextInput, View} from "react-native";
import {widthPercentageToDP} from "react-native-responsive-screen";
import Color from "../config/styles/Color";


export default class InputField extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            active: false
        }
    }

    onFocus() {
        this.setState({active: true});
        if (this.props.onFocus)
            this.props.onFocus();
    }

    onBlur() {
        this.setState({active: false});
        if (this.props.onBlur)
            this.props.onBlur();
    }

    render() {
        return (

            <View style={[styles.container, this.props.containerStyle]}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    {
                        this.props.left &&
                        <View style={[this.props.leftStyle, styles.leftStyle]}>
                            {this.props.left}
                        </View>
                    }
                    <TextInput
                        onChangeText={this.props.onChangeText}
                        style={[styles.inputField, this.props.inputField]}
                        placeholder={this.props.placeholder}
                        underlineColorAndroid={"transparent"}
                        placeholderTextColor={Color.SecondaryText}
                        value={this.props.value}
                        keyboardType={this.props.keyboardType ? this.props.keyboardType : "default"}
                        secureTextEntry={this.props.secureTextEntry ? this.props.secureTextEntry : false}
                        multiline={this.props.multiline}
                        numberOfLines={this.props.numberOfLines ? 5 : 1}
                        onBlur={this.onBlur.bind(this)}
                        onFocus={this.props.onFocus}
                        editable={this.props.editable}
                        returnKeyType={this.props.returnKeyType}
                        onSubmitEditing={this.props.onSubmitEditing}
                        autoFocus={this.props.autoFocus}
                    />
                </View>
                {
                    this.props.right &&
                    <TouchableOpacity style={[this.props.rightStyle, styles.rightStyle]}>
                        {this.props.right}
                    </TouchableOpacity>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        marginVertical: 10,
        height: widthPercentageToDP("10%"),
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
        borderColor: '#BCBCBC',
        // paddingHorizontal: 10,
        justifyContent: 'space-between'


    },
    leftStyle: {}
    ,
    inputField: {
        fontWeight: "300",
        fontSize: widthPercentageToDP("3.5%"),
        color: Color.SecondaryText,
        flex: 1
    },
    inputLabel: {
        color: "#969696",
        fontSize: 18
    },
    rightStyle: {}
});
