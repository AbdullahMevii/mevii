import React from 'react';
import {View, Text,TouchableOpacity} from 'react-native';
import RF from "react-native-responsive-fontsize";
import ResponsiveText from "./ResponsiveText";
import Fonts from "../config/styles/Fonts";

class CustomButton extends React.Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <View style={{
                    width: RF(20),
                    height: RF(6),
                    backgroundColor: this.props.text === "Iniciar" ? 'white' : '#FDD963',
                    borderRadius: RF(0.7),
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <ResponsiveText style={style.text}>{this.props.text} </ResponsiveText>
                </View>
            </TouchableOpacity>
        );
    }
}

const style = {
    text: {
        color: '#608BBE',
        fontSize: Fonts.h2,
        fontWeight: 'bold',

    }
}
export default CustomButton;