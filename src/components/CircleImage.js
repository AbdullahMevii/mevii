import React from 'react';
import {Image, View,TouchableOpacity} from 'react-native';
import RF from "react-native-responsive-fontsize";
import {Actions} from 'react-native-router-flux';


class CircleImage extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
      <View style={[styles.container,this.props.containerStyle]}>
        <Image source={this.props.Image}
               style={[styles.img,this.props.ImageStyle]}/>
      </View>
      </TouchableOpacity>
    );
  }
}

const styles = {
  container:{
    justifyContent: 'center',
    alignItems: 'center',
    width: RF(6),
    height: RF(6),
    borderRadius: RF(4),
    marginRight: RF(1),
    backgroundColor: 'white',
    elevation: 2,
    shadowOpacity:1,
    shadowOffset:{width:1,height:1}
  },
  img:{
    width: RF(5),
    height: RF(5),
    resizeMode: 'contain'
  }
}
export default CircleImage;