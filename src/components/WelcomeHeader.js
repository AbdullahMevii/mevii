import React from 'react';
import {View, Image, Text, StatusBar, SafeAreaView, Dimensions} from 'react-native';
import RF from 'react-native-responsive-fontsize';
import ResponsiveText from '../components/ResponsiveText';
import Fonts from '../config/styles/Fonts';

const {width} = Dimensions.get('window');

class WelcomeHeader extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../../assets/logo.png')} style={styles.logo}/>
        <ResponsiveText style={styles.text}>Bienvenido a Mevii</ResponsiveText>
      </View>
    );
  }
}

export default WelcomeHeader;
const styles = {
  container: {
    /*backgroundColor:'black',*/
    flex: 2,
    alignItems: 'center',
    width: '80%'
  },
  text: {
    marginTop: RF(1),
    color: 'white',
    fontWeight: 'bold',
    fontSize: Fonts.h1,
  },

  logo: {
    width: width * 0.3,
    height: (width * 0.3) / 2,
    resizeMode: 'contain'
  }
}