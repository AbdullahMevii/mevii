import React from 'react';
import {Image, View} from 'react-native';
import RF from "react-native-responsive-fontsize";
import ResponsiveText from "./ResponsiveText";

class LogoName extends React.Component {
  render() {
    return (
      <View style={this.props.containerStyle}>
        <Image style={{height: RF(12), width: RF(12), resizeMode: 'contain'}}
               source={require('../../assets/logo_white.png')}/>
        <ResponsiveText style={this.props.textStyle}>{this.props.text}</ResponsiveText>
      </View>
    );
  }
}

export default LogoName;
