import React from 'react';
import {Image, View, Text} from 'react-native';
import ResponsiveText from "./ResponsiveText";
import Color from "../config/styles/Color";
import RF from "react-native-responsive-fontsize";
import Fonts from "../config/styles/Fonts";

class FilterMini extends React.Component {
  state = {leftButton: false, rightButton: true}

  render() {
    return (
      <View style={styles.container}>
        <ResponsiveText style={{color: Color.Placeholder, fontWeight: 'bold'}}>Filtro</ResponsiveText>
        <View style={styles.bottomContainer}>
          <ResponsiveText style={{backgroundColor: '#E9E9E9', color: Color.Placeholder, borderRadius: 2,fontSize: "3%"}}>Propiedades en
            alquiler </ResponsiveText>
          <View style={styles.bottomContainerRight}>
            <View style={styles.innerContainer}>
              <ResponsiveText style={styles.bottomContainerText}>3</ResponsiveText>
              <Image source={require('../../assets/location.png')} style={styles.bottomContainerRightImage}/>
            </View>
            <View style={styles.innerContainer}>
              <ResponsiveText style={styles.bottomContainerText}>3</ResponsiveText>
              <Image source={require('../../assets/location.png')} style={styles.bottomContainerRightImage}/>
            </View>
            <ResponsiveText style={styles.bottomContainerText}>de 10K a 40K DOP </ResponsiveText>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    paddingHorizontal: RF(0.1),
    width: '100%',
    alignSelf: 'center',
  },
  bottomContainer: {
    flexDirection: 'row',
    marginTop: RF(0.6)
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottomContainerText: {
    fontSize: Fonts.h5,
    color: Color.Placeholder,
  },
  bottomContainerRightImage: {
    width: RF(2.5),
    height: RF(2.5)
  },
  bottomContainerRight: {
    marginLeft: RF(1),
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal: RF(2),
  }
}

export default FilterMini;