import React from 'react';
import {View, Image, TouchableOpacity, ImageBackground} from 'react-native';
import RF from "react-native-responsive-fontsize";
import ResponsiveText from "./ResponsiveText";
import Fonts from "../config/styles/Fonts";
import Color from "../config/styles/Color";

class CardSection extends React.Component {
  render() {
    const {item} = this.props;
    const imageUri = item.images.length > 0 ? `https://api.mevii.com/property/image/${item.images[0]}/thumbnail` : ""
    return (
      <TouchableOpacity onPress={() => this.props.onPress(item)}>
        <View style={[styles.container, this.props.style]}>
          <View style={styles.containerTop}>
            <ImageBackground style={styles.backgroundImage}
                             source={{uri: imageUri}}>
              <View style={{flexDirection: 'row'}}>
                <ResponsiveText style={{fontWeight: 'bold', fontSize: Fonts.h2}}>{item.currency}</ResponsiveText>
                <ResponsiveText
                  style={{marginLeft: RF(1), fontWeight: 'bold', fontSize: Fonts.h2}}>{item.price}</ResponsiveText>
              </View>

              {
                item.tags.length > 0 && <ResponsiveText style={{
                  backgroundColor: '#1E5EB6',
                  color: 'white',
                  fontSize: Fonts.h4,
                  borderRadius: 5,
                  paddingVertical: 2,
                  paddingHorizontal: 10,
                }}>{item.tags[0]}</ResponsiveText>
              }
            </ImageBackground>
          </View>

          <View style={styles.containerBottom}>

            <View style={styles.containerBottomInnerLeft}>
              <Image style={{width: RF(2.5), height: RF(2.5)}} source={require('../../assets/location.png')}/>
              <ResponsiveText style={styles.bottomContainerText}>{item.city} </ResponsiveText>
            </View>

            <View style={styles.containerBottomRight}>
              <View style={styles.containerInnerBottomRight}>
                <Image source={require('../../assets/location.png')} style={styles.bottomContainerRightImage}/>
                <ResponsiveText style={styles.bottomContainerText}>{item.bed_rooms}</ResponsiveText>
              </View>


              <View style={styles.containerInnerBottomRight}>
                <Image source={require('../../assets/location.png')} style={styles.bottomContainerRightImage}/>
                <ResponsiveText style={styles.bottomContainerText}>{item.bath_rooms}</ResponsiveText>
              </View>

              <View style={styles.containerInnerBottomRight}>
                <Image source={require('../../assets/location.png')} style={styles.bottomContainerRightImage}/>
                <ResponsiveText style={styles.bottomContainerText}>{item.square_metres}m</ResponsiveText>
                <ResponsiveText
                  style={{color: Color.Placeholder, fontSize: Fonts.h5, marginBottom: RF(2)}}>2 </ResponsiveText>
              </View>
            </View>


          </View>


        </View>
      </TouchableOpacity>
    )
  }
}

export default CardSection;

const styles = {
  container: {
    width: '85%',
    height: RF(23),
    backgroundColor: 'grey',
    alignSelf: 'center',
    borderRadius: RF(2),
  },
  containerTop: {
    overflow: 'hidden',
    flexDirection: 'row',
    flex: 2.5,
    borderTopRightRadius: RF(2),
    borderTopLeftRadius: RF(2),
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingHorizontal: RF(1),
    paddingBottom: RF(1),
  },
  containerBottom: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: RF(1),
    flexDirection: 'row',
    borderBottomLeftRadius: RF(2),
    borderBottomRightRadius: RF(2)
  },
  containerInnerBottomRight: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottomContainerText: {
    color: Color.Placeholder,
    fontSize: Fonts.h4
  },
  containerBottomInnerLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1.5
  },
  containerBottomRight: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  bottomContainerRightImage: {
    width: RF(2.5),
    height: RF(2.5)
  }
}
