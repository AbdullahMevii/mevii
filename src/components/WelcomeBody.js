import React from 'react';
import {View, Text} from 'react-native';
import RF from "react-native-responsive-fontsize";
import ResponsiveText from "./ResponsiveText";
import Fonts from "../config/styles/Fonts";

class WelcomeBody extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ResponsiveText style={styles.text}>
          Una experiencia diferente al{"\n"} momento de buscar tu {"\n"} nuevo
          espacio
        </ResponsiveText>
      </View>
    );
  }
}

const styles = {
  container: {
    /*backgroundColor:'grey',*/
    flex: 2,
    alignItems: 'center',
    width: '80%'
  },
  text: {
    color: 'white',
    textAlign: 'center',
    fontSize: '5.5%',
    fontWeight: 'bold',
  }
};

export default WelcomeBody;