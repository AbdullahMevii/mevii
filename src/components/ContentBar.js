import React from 'react';
import {Image, View,TouchableOpacity} from "react-native";
import RF from "react-native-responsive-fontsize";
import ResponsiveText from "./ResponsiveText";
import Fonts from "../config/styles/Fonts";

class ContentBar extends React.Component {
  render() {
    return (

      <TouchableOpacity onPress={()=>this.props.onPress(this.props.prediction)} disabled={this.props.touchable} >
      <View style={[styles.container, this.props.containerStyle]}>
        {
          this.props.Image && <Image style={[styles.img, this.props.imageStyle]}
                                     source={this.props.Image}/>
        }
        <ResponsiveText style={{color: '#7EAADA',marginLeft: RF(3),fontSize:Fonts.h4}}>{this.props.Text} </ResponsiveText>
      </View>
      </TouchableOpacity>
    );
  }
}

const styles = {
  container: {
    borderBottomWidth: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
    height: RF(10)
  },
  img: {
    tintColor: '#7EAADA',
    width: RF(4), height: RF(4),
    marginLeft: RF(3),
    marginTop: RF(3),
    marginBottom:RF(3)


  }

}
export default ContentBar;
