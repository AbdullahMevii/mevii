import React from 'react';
import {ImageBackground, StatusBar, SafeAreaView} from 'react-native';
import WelcomeHeader from "../components/WelcomeHeader";
import WelcomeBody from "../components/WelcomeBody";
import WelcomeFooter from "../components/WelcomeFooter";
import {Actions} from 'react-native-router-flux';
import RF from "react-native-responsive-fontsize";

class Welcome extends React.Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
          <StatusBar backgroundColor="#1E5EB6" barStyle="light-content"/>
          <WelcomeHeader/>
          <WelcomeBody/>
          <WelcomeFooter
            Registrar={() => Actions.Registrar()}
            LogIn={() => Actions.LogIn()}
          />
      </SafeAreaView>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: "#1E5EB6",
    paddingTop: RF(5)
  },
  imgBackground: {
  }
};

export default Welcome;