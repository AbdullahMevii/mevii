import React from 'react';
import {View, Text, TouchableOpacity, Image, StatusBar, ImageBackground} from 'react-native';
import HeaderImageScrollView, {TriggeringView} from 'react-native-image-header-scroll-view';
import RF from "react-native-responsive-fontsize";
import ResponsiveText from "../components/ResponsiveText";
import Color from "../config/styles/Color";
import Fonts from "../config/styles/Fonts";
import CircleImage from "../components/CircleImage";
import {Actions} from "react-native-router-flux";

class CardDetail extends React.Component {
  render() {
    const {data} = this.props;
    return (
      <View style={{flex: 1}}>
        <StatusBar
          backgroundColor={'#FA3D4C'}
          barStyle={'light-content'}
        />
        <HeaderImageScrollView
          headerContainerStyle={{}}
          maxHeight={RF(35)}
          minHeight={RF(10)}
          headerImage={{uri: `https://api.mevii.com/property/image/${data.images[0]}/thumbnail`}}
          renderFixedForeground={() => (

            <View style={styles.renderForegroundContainer}>

              <View style={styles.innerTopContainer}>
                <TouchableOpacity onPress={() => Actions.pop()}>
                  <Image source={require('../../assets/close.png')}
                         style={{tintColor: 'white', width: RF(4.5), height: RF(4.5)}}/>
                </TouchableOpacity>
                <Image source={require('../../assets/scan.png')}
                       style={{tintColor: 'white', width: RF(3.5), height: RF(3.5)}}/>
              </View>

              <View style={styles.innerBottomContainer}>
                <Image source={require('../../assets/share.png')}
                       style={styles.innerBottomContainerImage}/>
                <Image source={require('../../assets/heart.png')}
                       style={styles.innerBottomContainerImage}/>
              </View>

            </View>
          )}>
          <View style={{height: RF(150), paddingTop: RF(1)}}>
            <TriggeringView style={{paddingHorizontal: RF(2)}}>

              <View style={styles.triggerView1}>
                <Image source={require('../../assets/location.png')}
                       style={styles.triggerView1Img}
                />
                <ResponsiveText style={styles.triggerView1Text}>{data.city} </ResponsiveText>

              </View>


              <View style={styles.containerBottomRight}>
                <View style={styles.containerInnerBottomRight}>
                  <Image source={require('../../assets/location.png')} style={styles.bottomContainerRightImage}/>
                  <ResponsiveText style={styles.bottomContainerText}>{data.bed_rooms}</ResponsiveText>

                </View>


                <View style={styles.containerInnerBottomRight}>
                  <Image source={require('../../assets/location.png')} style={styles.bottomContainerRightImage}/>
                  <ResponsiveText style={styles.bottomContainerText}>{data.bath_rooms}</ResponsiveText>

                </View>

                <View style={styles.containerInnerBottomRight}>
                  <Image source={require('../../assets/location.png')} style={styles.bottomContainerRightImage}/>
                  <ResponsiveText style={styles.bottomContainerText}>{data.square_metres}m</ResponsiveText>
                  <ResponsiveText
                    style={{color: Color.Placeholder, fontSize: Fonts.h5, marginBottom: RF(2)}}>2 </ResponsiveText>


                </View>
              </View>


              <ResponsiveText style={styles.paragraph}>
                {data.description}
              </ResponsiveText>

              <ResponsiveText
                style={{marginVertical: RF(1), color: Color.Placeholder, fontWeight: 'bold', fontSize: Fonts.h3}}>Caracteristicas
                destacadas</ResponsiveText>
              {
                data.tags != [] &&
                <View style={{flexDirection: 'row', marginBottom: RF(2)}}>
                  {data.tags.map((text) => <ResponsiveText style={styles.triggerView5}>{text}</ResponsiveText>)}
                </View>
              }
              <View style={{flexDirection: 'row'}}>

                <View style={styles.triggerView6Inner}>
                  <CircleImage
                    containerStyle={{
                      backgroundColor: '#D7D7D7',
                      borderColor: 'grey',
                      borderWidth: 1,
                      marginRight: RF(2)
                    }}
                  />
                  <ResponsiveText style={{color: Color.Placeholder}}>Marvin Sena </ResponsiveText>
                </View>


                <View style={styles.triggerView6InnerRight}>
                  <ResponsiveText style={styles.triggerView6RightText}> Llamar</ResponsiveText>
                  <ResponsiveText style={styles.triggerView6RightText}> Chat</ResponsiveText>

                </View>
              </View>

            </TriggeringView>
            <View style={{marginTop: RF(1), borderColor: Color.Placeholder, borderWidth: 0.5, width: '100%'}}/>
            <View style={styles.triggerView8}>
              <View>
                <ResponsiveText style={{color: Color.Placeholder, fontSize: Fonts.h3}}>Apartamento en
                  venta</ResponsiveText>
                <ResponsiveText
                  style={{color: Color.Placeholder, fontWeight: 'bold', fontSize: Fonts.h2}}>DOP </ResponsiveText>
                <ResponsiveText style={{
                  color: Color.Placeholder,
                  fontWeight: 'bold',
                  fontSize: Fonts.h2
                }}>{data.price} </ResponsiveText>

              </View>

              <ResponsiveText style={styles.triggerView8RightText}>Calcular Hipoteca
              </ResponsiveText>

            </View>
          </View>
        </HeaderImageScrollView>
      </View>
    );
  }
}

const styles = {
  renderForegroundContainer: {
    width: '100%',
    marginTop: RF(2),
    height: '100%',
    paddingHorizontal: RF(2)
  },
  innerTopContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: RF(3)
  },
  innerBottomContainer: {
    top: RF(19),
    flexDirection: 'row',
    width: '20%',
    justifyContent: 'space-between',
    alignSelf: 'flex-end'
  },
  innerBottomContainerImage: {
    tintColor: 'white',
    width: RF(4),
    height: RF(4)
  },
  containerBottomRight: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '60%',
    marginVertical: RF(0.5),
    /*backgroundColor:'blue'*/
  },
  containerInnerBottomRight: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottomContainerText: {
    color: Color.Placeholder,
    fontSize: Fonts.h4
  },
  bottomContainerRightImage: {
    width: RF(2.5),
    height: RF(2.5),
    marginRight: RF(1.5)
  },
  triggerView1: {
    flexDirection: 'row',
    alignItems: 'center',
    /*backgroundColor:'yellow'*/
  },
  triggerView1Img: {
    width: RF(3),
    height: RF(3),
    marginRight: RF(0.8)
  },
  triggerView1Text: {
    color: Color.Primary,
    fontSize: Fonts.h2
  },
  paragraph: {
    fontSize: Fonts.h4,
    color: Color.Placeholder,
    textAlign: 'justify'
  },
  triggerView5: {
    borderRadius: RF(1.3),
    paddingHorizontal: RF(1),
    marginRight: RF(1),
    backgroundColor: Color.Primary,
    fontSize: Fonts.h4,
    fontWeight: 'bold'
  },
  triggerView6Inner: {
    flexDirection: 'row',
    alignItems: 'center',
    /*backgroundColor:'red',*/
    flex: 2
  },
  triggerView6InnerRight: {
    flexDirection: 'row',
    /*backgroundColor:'yellow',*/
    alignItems: 'center',
    flex: 1,
    justifyContent: 'space-between'
  },
  triggerView6RightText: {
    color: Color.Primary,
    textDecorationLine: 'underline',
    textDecorationColor: Color.Placeholder,
    fontSize: Fonts.h3
  },
  triggerView8: {
    flexDirection: 'row',
    paddingHorizontal: RF(2),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  triggerView8RightText: {
    borderRadius: RF(1.3),
    padding: RF(1),
    backgroundColor: Color.Primary,
    fontSize: Fonts.h4,
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center'
  }

}
export default CardDetail;