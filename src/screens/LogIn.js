import React from 'react';
import {
  View,
  Image,
  Keyboard,
  StatusBar,
  TextInput,
  AsyncStorage,
  SafeAreaView,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import Header from "../components/Header";
import LogoName from "../components/LogoName";
import Fonts from "../config/styles/Fonts";
import RF from "react-native-responsive-fontsize";
import Button from "../components/Button";
import Color from "../config/styles/Color";
import ResponsiveText from "../components/ResponsiveText";
import axios from 'axios';
import {API} from '../config/Constants';
import {widthPercentageToDP} from "react-native-responsive-screen";
import {Actions} from "react-native-router-flux";
import {GoogleSignIn} from "../config/services/GoogleSignIn";
import {FacebookSignIn} from "../config/services/FacebookSignIn";
import Content from "../components/Content";

class LogIn extends React.Component {
  state = {email: '', password: '', errorMessage: '', loading: false, keyboardActive: false}

  async componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => this.setState({keyboardActive: true}),
    );

    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => this.setState({keyboardActive: false}),
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  onLogInPress() {
    this.setState({loading: true});

    let {email, password} = this.state;
    let body = {
      username: email,
      password: password
    }

    if (email === "") {
      this.setState({errorMessage: "Email is missing", loading: false});

    } else if (password === "") {
      this.setState({errorMessage: "Password is missing", loading: false})
    } else {
      axios.post(`${API}user/login`, body)
        .then(res => {
          console.warn("Login response" + res);
          res = {...res, loggedInWith: "api"}
          AsyncStorage.setItem("USER", JSON.stringify(res)).then(res => console.warn("SET user on login")).catch(error => console.warn("Error on login setitem"));
          Actions.Home(res);
        })
        .catch(err => {
          alert(err.response.data.message);
        })
        .finally(() => {
          this.setState({loading: false})
        });
    }
  }

  getErrorMessage() {
    if (this.state.errorMessage != "") {
      alert(this.state.errorMessage);
      this.setState({errorMessage: ''})
    }
  }

  onGoogleSignInPress() {
    GoogleSignIn(
      (success) => {
        success = {...success, loggedInWith: "google"}
        AsyncStorage.setItem("USER", JSON.stringify(success)).then(res => console.warn("SET googleuser on login")).catch(error => console.warn("Error on logingoogle setitem"));
        Actions.Home

        (success);
      },
      (error) => {
        alert("Google sign out error:" + error)

      }
    )
  }

  onFacebookSignInPress() {
    FacebookSignIn((success) => {
        success = {...success, loggedInWith: 'fb'};
        AsyncStorage.setItem("USER", JSON.stringify(success)).then(res => console.warn("SET fbuser on login")).catch(error => console.warn("Error on loginfb setitem"));
        Actions.Home();
      },
      (error) => {
        alert("Facebook sign in error:" + error);
      })
  }


  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        {/*<ScrollView style={{*/}
        {/*flex:1,*/}
        {/*minHeight: Dimensions.get('window').height*/}
        {/*}}>*/}
        <Content>
          <StatusBar backgroundColor="white" barStyle="dark-content"/>
          {this.getErrorMessage()}
          <Header
            onPress={() => Actions.pop()}
          />
          <LogoName
            containerStyle={styles.logo}
            text={'Inicia Sesion'}
            textStyle={styles.text}
          />

          <View style={styles.buttonContainer}>
            <Button
              btnText={styles.googleText}
              contentStyle={styles.google}
              containerStyle={{marginBottom: 10}}
              // containerStyle={styles.google}
              text={"Usando Google"}
              leftIcon={require('../../assets/google.png')}
              value={"Google"}
              onPress={this.onGoogleSignInPress.bind(this)}
            />
            <Button
              btnText={styles.facebookText}
              contentStyle={styles.facebook}
              containerStyle={{marginBottom: 10}}
              // containerStyle={styles.google}
              text={"Usando Facebook"}
              leftIcon={require('../../assets/facebook.png')}
              value={"Facebook"}
              onPress={this.onFacebookSignInPress.bind(this)}
            />
          </View>

          <View style={styles.styleContainer}>
            <Image style={styles.styleImage}
                   source={require('../../assets/divider_icon.png')}/>
          </View>

          <View style={styles.footerContainer}>
            <View style={styles.cardSection}>
              <View style={styles.imageInputContainer}>
                <Image style={styles.fieldImage} source={require('../../assets/email.png')}/>
                <TextInput
                  onChangeText={(email) => this.setState({email: email})}
                  value={this.state.email}
                  style={{flex: 1}}
                  placeholder={"Correo Electronico"}
                  onPress={() => null}
                  returnKeyType={"next"}
                  onSubmitEditing={() => {
                    this.passwordInput.focus();
                    Keyboard.dismiss();
                  }}
                />
              </View>

              <View style={styles.imageInputContainer}>
                <Image style={styles.fieldImage} source={require('../../assets/lock.png')}/>
                <TextInput
                  ref={(ref) => {
                    this.passwordInput = ref
                  }}
                  style={{flex: 1}}
                  secureTextEntry={true}
                  onChangeText={(password) => this.setState({password: password})}
                  value={this.state.password}
                  placeholder={"Contraseña"}
                  onPress={() => null}
                />
              </View>

              <Button
                loading={this.state.loading}
                text={"Iniciar"}
                leftIcon={null}
                contentStyle={styles.containerStyle}
                btnText={{fontSize: Fonts.h2}}
                onPress={this.onLogInPress.bind(this)}
              />
            </View>
          </View>

        </Content>
        {
          !this.state.keyboardActive &&
          <View style={styles.footer}>
            <View style={styles.footerContent}>
              <TouchableOpacity onPress={() => Actions.UserRegister()} style={styles.footerTouchable}>
                <ResponsiveText style={styles.footerText}>
                  Registrar con correo electronico
                </ResponsiveText>
              </TouchableOpacity>
              <View style={{flex: 1}}/>
              {/*<TouchableOpacity onPress={() => Actions.Register()} style={styles.footerTouchable}>*/}
              <ResponsiveText style={styles.footerText}>
                Olvide la contraseña
              </ResponsiveText>
              {/*</TouchableOpacity>*/}

            </View>
          </View>
        }

      </SafeAreaView>
    );
  }
}

export default LogIn;
const styles = {
  text: {
    fontSize: Fonts.h1,
    color: '#317BC6',
    fontWeight: 'bold',
    marginTop: RF(1.5)
  },
  logo: {
    alignItems: 'center'
  },
  google: {
    backgroundColor: '#DA4437',
    shadowOpacity: 2,
    shadowOffset: {width: 2, height: 2},
    elevation: RF(0.3),
    // marginBottom: RF(4),
    borderRadius: RF(1),
    paddingHorizontal: RF(3),
  },
  googleText: {
    color: 'white',
    textAlign: 'left',
    fontSize: Fonts.hMed,
  },
  facebook: {
    backgroundColor: '#3A5897',

    elevation: RF(0.3),
    shadowOpacity: 2,
    shadowOffset: {width: 2, height: 2},
    // marginBottom: RF(4),
    borderRadius: RF(1),
    paddingHorizontal: RF(3),
  },
  facebookText: {
    color: 'white',
    textAlign: 'left',
    fontSize: Fonts.hMed,
  },
  containerStyle: {
    justifyContent: 'center',
    /*shadowOpacity: 4,*/
    shadowOpacity: 2,
    shadowOffset: {width: 2, height: 2},
    elevation: RF(0.3),
    borderRadius: RF(1),
    backgroundColor: '#1E5EB6',
    width: "100%",
    marginVertical: RF(3)
  },
  buttonContainer: {
    marginVertical: RF(2.5)
  },
  styleContainer: {
    alignSelf: 'center',
    width: "80%",
    borderBottomWidth: RF(0.1),
    borderColor: Color.Placeholder,
    position: 'relative',
    justifyContent: 'center',
    marginBottom: RF(2)
  },
  styleImage: {
    width: RF(6),
    height: RF(6),
    resizeMode: "contain",
    alignSelf: 'center',
    position: 'absolute'
  },
  cardSection: {
    alignSelf: 'center',
    marginTop: RF(4),
    elevation: RF(0.3),
    shadowOpacity: 2,
    shadowOffset: {width: 2, height: 2},
    width: "80%",
    paddingHorizontal: RF(2),
    borderRadius: RF(2),
    backgroundColor: 'white'
  },
  imageInputContainer: {
    width: "100%",
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Color.Placeholder
  },
  fieldImage: {
    height: RF(3),
    width: RF(3),
    margin: RF(1),
    marginRight: RF(2)
  },
  footerInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: RF(1),
    alignSelf: 'flex-end',
    marginBottom: RF(1),
    flex: 1,
    alignItems: 'flex-end'
  },
  footerContainer: {
    justifyContent: 'space-between',
    flex: 1,
    paddingBottom: RF(2)
  },
  footer: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: RF(2),
    marginHorizontal: 10,
  },
  footerContent: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',
    justifyContent: 'space-between',
  },
  footerTouchable: {
    flex: 2,
  },
  footerText: {
    fontSize: "4%",
    color: Color.SecondaryText,
    textDecorationLine: 'underline',
  },
  container: {
    marginVertical: 10,
    height: widthPercentageToDP("10%"),
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#BCBCBC',
    justifyContent: 'space-between',
    width: "80%",
  },
  leftStyle: {},
  inputField: {
    fontWeight: "300",
    fontSize: widthPercentageToDP("3.5%"),
    color: Color.SecondaryText,
    flex: 1
  },
  inputLabel: {
    color: "#969696",
    fontSize: 18
  },
  rightStyle: {}
};
