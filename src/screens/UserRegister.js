import React from 'react';
import {Actions} from 'react-native-router-flux';
import {View, AsyncStorage, ScrollView, StatusBar, TextInput, SafeAreaView, Dimensions, Keyboard} from 'react-native';
import Header from "../components/Header";
import RF from "react-native-responsive-fontsize";
import ResponsiveText from "../components/ResponsiveText";
import Fonts from "../config/styles/Fonts";
import Button from "../components/Button";
import LogoName from "../components/LogoName";
import CheckBox from "../components/CheckBox";
import Color from "../config/styles/Color";
import axios from 'axios';
import {API} from "../config/Constants";
import {localStorage} from "../config/Constants"
import {widthPercentageToDP} from "react-native-responsive-screen";
import Content from "../components/Content";

class UserRegister extends React.Component {
  state = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    verifyPassword: '',
    checkbox: true,
    errorMessage: "",
    loading: '',
    keyboardActive: false
  }


  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => this.setState({keyboardActive: true}),
    );

    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => this.setState({keyboardActive: false}),
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }


  onRegisterPress() {
    this.setState({loading: true});
    const {firstName, lastName, email, password, verifyPassword, checkbox} = this.state;
    const body = {
      firstName: firstName,
      lastName: lastName,
      password: password,
      email: email,

    }

    if (firstName === "") {
      this.setState({errorMessage: "First name is missing", loading: false})
    } else if (lastName === "") {
      this.setState({errorMessage: "Last name is missing", loading: false})
    } else if (email === "") {
      this.setState({errorMessage: "Email is missing", loading: false})
    } else if (password === "") {
      this.setState({errorMessage: "Password is missing", loading: false})
    } else if (password !== verifyPassword) {
      this.setState({errorMessage: "Password don't matched", loading: false})
    } else if (checkbox === false) {
      this.setState({errorMessage: "Kindly accept terms and conditions", loading: false})
    } else {
      axios
        .post(`${API}user/register`, body)
        .then(res => res.data)
        .then(response => {
            AsyncStorage.setItem(localStorage.USER, JSON.stringify(response))
              .then(Actions.Home())
              .catch(err => console.warn(err))
          }
        )
        .catch(err => {
          alert("Email Already exist");
        })
        .finally(() => this.setState({loading: false}))

    }

  }

  getErrorMessage() {
    if (this.state.errorMessage !== "") {
      alert(this.state.errorMessage);
      this.setState({errorMessage: ""})
    }


  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        <StatusBar backgroundColor="white" barStyle="dark-content"/>


        <Content>

          <Header
            onPress={() => Actions.pop()}
          />
          <LogoName
            containerStyle={styles.logo}
            text={'Registro de usuario'}
            textStyle={styles.text}
          />
          <View style={styles.inputFieldContainer}>
            <View style={{flexDirection: 'row',}}>

              <View style={styles.container}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>

                  <TextInput
                    style={styles.inputField}
                    onChangeText={(firstName) => this.setState({firstName: firstName})}
                    value={this.state.firstName}
                    placeholder={"Nombre"}
                    onPress={() => null}
                    returnKeyType={'next'}
                    onSubmitEditing={() => {
                      this.lastName.focus();
                    }}
                  />
                </View>
              </View>


              <View style={[styles.container, {
                flex: 1,
                marginLeft: RF(1)
              }]}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <TextInput
                    style={styles.inputField}
                    onChangeText={(lastName) => this.setState({lastName: lastName})}
                    value={this.state.lastName}
                    placeholder={"Apellido"}
                    onPress={() => null}
                    returnKeyType={'next'}
                    onSubmitEditing={() => {
                      this.email.focus();
                    }}
                    ref={(ref) => {
                      this.lastName = ref
                    }}

                  />

                </View>
              </View>

            </View>

            <View style={styles.container}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TextInput
                  style={styles.inputField}
                  onChangeText={(email) => this.setState({email: email})}
                  value={this.state.email}
                  placeholder={"Correo Electronico"}
                  onPress={() => null}
                  ref={(ref) => this.email = ref}
                  returnKeyType={'next'}
                  onSubmitEditing={() => {
                    this.password.focus();
                  }}
                />

              </View>
            </View>


            <View style={styles.container}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TextInput
                  style={styles.inputField}
                  onChangeText={(password) => this.setState({password: password})}
                  value={this.state.password}
                  secureTextEntry={true}
                  placeholder={"Contraseña"}
                  onPress={() => null}
                  ref={(ref) => this.password = ref}
                  returnKeyType={'next'}
                  onSubmitEditing={() => {
                    this.verifyPassword.focus();
                    Keyboard.dismiss();
                  }}

                />
              </View>
            </View>


            <View style={styles.container}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TextInput
                  style={styles.inputField}
                  onChangeText={(verifyPassword) => this.setState({verifyPassword: verifyPassword})}
                  value={this.state.verifyPassword}
                  secureTextEntry={true}
                  placeholder={"Veriﬁcar Contraseña"}
                  onPress={() => null}
                  ref={(ref) => {
                    this.verifyPassword = ref
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                  }}
                />
              </View>
            </View>
          </View>
          <View style={styles.agreementContainer}>
            <CheckBox
              value={this.state.checkbox}
              onPress={() => this.setState({checkbox: !this.state.checkbox})}
            />
            <ResponsiveText style={{color: Color.Placeholder, fontSize: Fonts.h4}}>Conﬁrmo que he leído y
              acepto
              la política de privacidad y los Términos y condicionesy que deseo recibir correos
              electrónicos
              sobre actualizaciones de Mevii y otras ofertas</ResponsiveText>
          </View>
          <Button
            loading={this.state.loading}
            text={"Iniciar"}
            leftIcon={null}
            contentStyle={styles.containerStyle}
            btnText={{fontSize: Fonts.h2}}
            onPress={this.onRegisterPress.bind(this)}
          />
          {this.getErrorMessage()}

        </Content>
        {
          !this.state.keyboardActive &&
          <View style={styles.footer}>
            <View style={styles.footerContent}>
              <ResponsiveText style={styles.footerText}>
                Registrar con correo electronico
              </ResponsiveText>
              <View style={{flex: 1}}/>
              <ResponsiveText style={styles.footerText}>
                Olvide la contraseña
              </ResponsiveText>
            </View>
          </View>
        }

      </SafeAreaView>
    );
  }
}

export default UserRegister;
const styles = {
  text: {
    fontSize: Fonts.h1,
    color: '#317BC6',
    fontWeight: 'bold',
    marginTop: RF(1.5)
  },
  inputFieldContainer: {

    width: '80%',
    alignSelf: 'center',
    marginTop: RF(2)
  },
  logo: {

    alignItems: 'center'
  },
  agreementContainer: {
    width: "80%",
    alignSelf: 'center',
    flexDirection: 'row'
  },
  containerStyle: {
    justifyContent: 'center',
    shadowOpacity: 2,
    shadowOffset: {width: 2, height: 2},
    elevation: RF(0.3),
    borderRadius: RF(1),
    backgroundColor: '#1E5EB6',
    marginTop: 20
  },
  footerContainer: {
    flex: 1,
    flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: "flex-end",
    // alignSelf: "flex-end",
    // backgroundColor: 'red',
    paddingHorizontal: RF(2)
  },

  container: {
    flex: 1,
    marginVertical: RF(1),
    height: widthPercentageToDP("10%"),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#BCBCBC',
    // paddingHorizontal: 10,
    justifyContent: 'space-between'
  },
  footer: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: RF(2),
    marginHorizontal: 10,
  },
  footerContent: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',
    justifyContent: 'space-between',
  },
  footerText: {
    flex: 2,
    fontSize: "4%",
    color: Color.SecondaryText,
    textDecorationLine: 'underline',
  },
  leftStyle: {},
  inputField: {
    fontWeight: "300",
    fontSize: widthPercentageToDP("3.5%"),
    color: Color.SecondaryText,
    flex: 1
  },
  inputLabel: {
    color: "#969696",
    fontSize: 18
  },
  rightStyle: {}
}
