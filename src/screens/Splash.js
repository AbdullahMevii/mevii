import React from 'react';
import {Actions} from 'react-native-router-flux'
import {Dimensions, Image, AsyncStorage, StatusBar, SafeAreaView} from 'react-native';
import {STORAGE_KEY} from "../config/Constants";

const {width} = Dimensions.get('window');

class Splash extends React.Component {
  async componentDidMount() {
    // await AsyncStorage.clear();
    let user = await AsyncStorage.getItem(STORAGE_KEY.USER);
    user ? Actions.Home() : Actions.Welcome()
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="#1E5EB6" barStyle="light-content"/>
        <Image source={require('../../assets/logo.png')} style={styles.logo}/>
      </SafeAreaView>
    );
  }
}

export default Splash;
const styles = {
  container: {
    flex: 1,
    backgroundColor: '#1E5EB6',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: width * 0.6,
    height: width * 0.4,
    resizeMode: 'contain'
  }

}
