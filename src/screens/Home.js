import React from "react";
import {
  View,
  StatusBar,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  ActivityIndicator
} from "react-native";
import RF from "react-native-responsive-fontsize";
import CardSection from "../components/CardSection";
import SwipeUpDown from "react-native-swipe-up-down/src";
import FilterFull from "../components/FilterFull";
import FilterMini from "../components/FilterMini";
import { Actions } from "react-native-router-flux";
import axios from "axios";
import { API } from "../config/Constants";
import ResponsiveText from "../components/ResponsiveText";
import Color from "../config/styles/Color";

//Added
import ClusteredMapView from "react-native-maps-super-cluster";
import { Marker } from "react-native-maps";
import { Text } from "react-native";
import geoViewport from "@mapbox/geo-viewport";

import {
  getRegionByZoomLevel,
  getRegionBoundaries,
  getZoomLevelFromRegion
} from "../mapUtils";

let globalID = 0;

function hexToRgbA(hex, opacity) {
  let c;
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    c = hex.substring(1).split("");
    if (c.length == 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]];
    }
    c = "0x" + c.join("");
    return (
      "rgba(" +
      [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(",") +
      "," +
      opacity.toString() +
      ")"
    );
  }
  throw new Error("Bad Hex");
}

const { width } = Dimensions.get("window");
const MIN_DELTA_FOR_CIRCLE_MARKER = 10;

class Home extends React.Component {
  state = {
    renderHeader: true,
    currentLocation: null,
    cardDataDocs: [],
    destinationLocation: null,
    apiResult: null,
    delta: { lonDelta: 0.0121, latDelta: 0.015 },
    zoom: null,
    requesting: false,
    locationUpdated: false,
    layout: null,
    points: []
  };

  async componentWillMount() {
    if (this.props.searchedResults != null) {
      this.setState({
        cardDataDocs: this.props.searchedResults,
        region: this.props.location,
        currentLocation: this.props.location
      });
    } else if (this.props.currentLocation != null) {
      await navigator.geolocation.getCurrentPosition(
        position => {
          let location = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
          };
          this.setState({
            currentLocation: location,
            region: location
          });
        },
        error => console.warn(error),
        {}
      );
    } else {
      await navigator.geolocation.getCurrentPosition(
        position => {
          let location = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
          };
          this.setState({
            currentLocation: location,
            region: location
          });
        },
        error => console.warn(error),
        {}
      );
    }
  }

  getZoomLevel() {
    const { region, layout } = this.state;

    if (!region || !layout) return null;

    return getZoomLevelFromRegion(region, layout);
  }

  renderNextZoomRegion() {
    const zoomLevel = this.getZoomLevel();

    if (!zoomLevel) {
      return null;
    }

    const { latitude, longitude } = this.state.region;
    const { width, height } = this.state.layout;
    const bounds = getRegionBoundaries(
      { latitude, longitude },
      Math.floor(zoomLevel) + 1,
      { width, height }
    );

    // console.warn(bounds);

    return bounds;
  }

  onRegionChange(region) {
    this.setState({ region, locationUpdated: true });
  }

  requestNewProperties = () => {
    this.setState({ requesting: true, locationUpdated: false });
    this.getProperties();
  };

  constructor(props) {
    super();
    this._mapview = React.createRef();
  }

  getProperties() {
    const { region } = this.state;

    // console.warn(this._mapview);

    let bounds = this.renderNextZoomRegion();

    let request_params = {
      query: {
        bottom_left: {
          lat: bounds.southLatitude,
          lon: bounds.westLongitude
        },
        center: {
          lat: region.latitude,
          lon: region.longitude
        },
        top_right: {
          lat: bounds.northLatitude,
          lon: bounds.eastLongitude
        }
      },
      currency: "DOP",
      page: 1,
      limit: 20,
      sort: {
        _id: -1
      }
    };

    ("-------- UNCOMENT THIS IF YOU WANT TO SHOW ALL POINTS AVAILABLE FOR REGION -----");
    ("-------------------------- AND CHANGE MAP PINS TO POINTS VARIABLE -----------------");
    // axios
    //   .post(`${API}property/searchMap`, request_params)
    //   .then(res => res.data)
    //   .then(res => {
    //     console.warn(res);
    //     if (res.docs && res.docs.length > 0) {
    //       // console.warn("before points");

    //       let points = res.docs.map(point => {
    //         return point.reduce((prev, current, index) => {
    //           prev[res.headers[index]] = current;
    //           return prev;
    //         }, {});
    //       });
    //       // console.warn(points);
    //       return Promise.resolve(points);
    //     }

    //     return Promise.resolve([]);
    //   })
    // .then(points =>
    axios
      .post(`${API}property/search`, request_params)
      .then(res => res.data)
      .then(res => {
        // console.warn(res.docs ? res.docs.length : "NO DOCUMENTS");
        if (res.docs.length > 0) {
          this.setState({
            cardDataDocs: res.docs
            // points
          });
        }
      })
      .catch(err => console.warn(err))
      .finally(() => this.setState({ requesting: false }));
    // )
    // .catch(err => console.warn(err))
    // .finally(() => this.setState({ requesting: false }));
  }

  renderCluster = (cluster, onPress) => {
    const pointCount = cluster.pointCount,
      coordinate = cluster.coordinate,
      clusterId = cluster.clusterId;

    let maincolor;
    if (pointCount > 49) {
      maincolor = "#f90610";
    } else if (pointCount > 9) {
      maincolor = "#fac007";
    } else {
      maincolor = "#068af9";
    }

    return (
      <Marker
        identifier={`cluster-${clusterId}`}
        coordinate={coordinate}
        onPress={onPress}
      >
        <View
          style={[
            {
              borderColor: hexToRgbA(maincolor, 0.05)
            },
            styles.borders
          ]}
        >
          <View
            style={[
              {
                borderColor: hexToRgbA(maincolor, 0.2)
              },
              styles.borders
            ]}
          >
            <View
              style={[
                {
                  borderColor: hexToRgbA(maincolor, 0.6)
                },
                styles.borders
              ]}
            >
              <View
                style={[
                  styles.clusterContainer,
                  { backgroundColor: maincolor }
                ]}
              >
                <Text style={styles.clusterText}>{pointCount}</Text>
              </View>
            </View>
          </View>
        </View>
      </Marker>
    );
  };

  renderMarker = pin => {
    // console.log("pin:", pin.id);
    return (
      <Marker
        identifier={`pin-${pin.id}`}
        key={pin.id}
        coordinate={pin.location}
      >
        <View
          style={{
            flexDirection: "column",
            alignSelf: "flex-start"
          }}
        >
          <View style={styles.marker}>
            <Text style={styles.markerText}>{pin.label}</Text>
          </View>
        </View>
      </Marker>
    );
  };

  getBoundingBox({ latitude, longitude }, zoom) {
    let x = Dimensions.get("window").width;
    let y = Dimensions.get("window").height;
    return geoViewport.bounds([latitude, longitude], zoom, [x, y]);
  }

  getMapView() {
    const { region, cardDataDocs } = this.state;

    let pins = cardDataDocs.map(item => {
      let priceString = "$" + (parseInt(item.price) / 1000).toFixed(1) + "k";
      // globalID++;
      return {
        id: `pin${item._id}`,
        location: {
          latitude: item.lat,
          longitude: item.lon
        },
        label: priceString
      };
    });

    if (!region) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: "#444"
          }}
        />
      );
    } else {
      return (
        <ClusteredMapView
          style={{ flex: 1 }}
          ref={this._mapview}
          data={pins}
          renderMarker={this.renderMarker}
          renderCluster={this.renderCluster}
          initialRegion={region}
          onRegionChangeComplete={this.onRegionChange.bind(this)}
          onLayout={e => this.setState({ layout: e.nativeEvent.layout })}
          radius={30}
          extend={250}
          onMapReady={this.getProperties.bind(this)}
        />
      );
    }
  }

  render() {
    return (
      <View style={{ flex: 1, position: "relative" }}>
        <StatusBar
          translucent={true}
          backgroundColor={"transparent"}
          barStyle={"dark-content"}
        />

        {this.getMapView()}

        {this.state.renderHeader && (
          <SafeAreaView
            style={[
              styles.headerContainer,
              { marginTop: StatusBar.currentHeight + 15 }
            ]}
          >
            <TouchableOpacity
              onPress={() => {
                this.setState({ region: null });
                Actions.Search();
              }}
              style={{ flex: 1 }}
            >
              <View style={styles.imageFieldContainer}>
                <Image
                  source={require("../../assets/search.png")}
                  style={{ width: RF(3), height: RF(3) }}
                />
                <ResponsiveText style={{ color: Color.Placeholder }}>
                  {" "}
                  Introduzca una ubicación
                </ResponsiveText>
              </View>
            </TouchableOpacity>

            {(this.state.locationUpdated || this.state.requesting) && (
              <TouchableOpacity
                style={styles.refreshPropertiesButton}
                onPress={this.requestNewProperties}
              >
                {(this.state.requesting && <ActivityIndicator />) || (
                  <Text style={styles.refreshPropertiesButtonText}>
                    Refresh the properties
                  </Text>
                )}
              </TouchableOpacity>
            )}
            <View
              style={[styles.circleContainer, { marginHorizontal: RF(1.5) }]}
            >
              <Image
                style={styles.circleContainerImage}
                source={require("../../assets/chat.png")}
              />
            </View>

            <View style={styles.circleContainer}>
              <Image
                style={styles.circleContainerImage}
                source={require("../../assets/profile.png")}
              />
            </View>
          </SafeAreaView>
        )}

        <View
          style={{
            position: "absolute",
            bottom: RF(8),
            left: RF(3),
            right: 0,
            marginBottom: RF(4)
          }}
        >
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {this.state.cardDataDocs.length > 0 &&
              this.state.cardDataDocs.map((data, index) => (
                <CardSection
                  key={index}
                  text={this.state.delta.lonDelta}
                  style={{ width: width * 0.9, marginRight: RF(2) }}
                  item={data}
                  onPress={data => Actions.CardDetail({ data: data })}
                />
              ))}
          </ScrollView>
        </View>

        <SwipeUpDown
          itemMini={<FilterMini />}
          itemFull={<FilterFull />}
          onShowMini={() => this.setState({ renderHeader: true })}
          onShowFull={() => this.setState({ renderHeader: false })}
          onMoveDown={() => console.warn("down")}
          onMoveUp={() => console.warn("up")}
          disablePressToShow={false} // Press item mini to show full
          style={{
            paddingTop: 0,
            backgroundColor: "white",
            marginHorizontal: RF(1.5),
            flex: 1
          }}
          swipeHeight={RF(11)}
        />
      </View>
    );
  }
}

const styles = {
  map: {
    flex: 1,
    position: "relative"
  },
  headerContainer: {
    width: "90%",
    position: "absolute",
    marginTop: RF(6),
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "center"
  },
  imageFieldContainer: {
    flexDirection: "row",
    backgroundColor: "white",
    alignItems: "center",
    flex: 1,
    borderRadius: RF(5),
    padding: RF(1),
    height: RF(7),
    elevation: 1,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 1
  },
  circleContainer: {
    elevation: 1,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 1,
    backgroundColor: "white",
    alignItems: "center",
    borderRadius: RF(5),
    width: RF(6.5),
    height: RF(6.5),
    justifyContent: "center"
  },
  circleContainerImage: {
    width: RF(4),
    height: RF(4)
  },
  customMarkerContainer: {
    // elevation: 1,
    shadowOpacity: 1,
    shadowOffset: { width: 1, height: 1 },
    justifyContent: "space-between",
    flexDirection: "row",
    padding: RF(1),
    marginBottom: 20,
    alignItems: "center",
    borderRadius: RF(1),
    borderWidth: 1,
    borderColor: "grey",
    backgroundColor: "white",
    position: "relative",
    zIndex: 5
  },
  markerImg: {
    marginRight: RF(2),
    width: RF(3),
    height: RF(3),
    tintColor: "red"
  },

  // ADDED
  //Cluster style
  clusterContainer: {
    width: 30,
    height: 30,
    padding: 4,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    margin: 2
  },
  clusterText: {
    fontSize: 14,
    fontWeight: "700",
    textAlign: "center"
  },
  borders: {
    borderWidth: 3,
    borderRadius: 50,
    margin: 2
  },

  //Marker style
  marker: {
    flexDirection: "row",
    alignSelf: "flex-start",
    backgroundColor: "#FFF",
    paddingHorizontal: 10,
    paddingVertical: 6,
    borderRadius: 3,
    borderColor: "#FFFFFF",
    borderWidth: 0.5
  },
  markerText: {
    color: "#7e7e7e",
    fontSize: 13,
    fontWeight: "200"
  },
  refreshPropertiesButton: {
    position: "absolute",
    top: 65,
    left: 30,
    right: 30,
    padding: 10,
    backgroundColor: "white",
    borderRadius: 20,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  refreshPropertiesButtonText: {
    color: "#000000"
  }
};

export default Home;
