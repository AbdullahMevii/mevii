import React from 'react';
import {Image, StatusBar, View, TouchableOpacity, AsyncStorage,SafeAreaView} from 'react-native';
import InputField from "../components/InputField";
import RF from "react-native-responsive-fontsize";
import ResponsiveText from "../components/ResponsiveText";
import ContentBar from "../components/ContentBar";
import CircleImage from "../components/CircleImage";
import Fonts from "../config/styles/Fonts";
import {Actions} from 'react-native-router-flux';
import RNGooglePlaces from 'react-native-google-places';
import axios from "axios";
import {API, STORAGE_KEY} from "../config/Constants";

/*const api={"lahore"}*/
class Search extends React.Component {
  state = {searchLocation: '', renderPredictions: [], lonlat: []}
  flag = false;

  async getPredictions(text) {
  await  axios.get(`https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyCfp9O4mgQSez58p_j5Dak_jEup3kSvHnc&input=${text}`)
      .then(response => response.data).then(response => this.setState({renderPredictions: response.predictions}))
      .catch(error => console.warn(error));

    // RNGooglePlaces.openAutocompleteModal(text)
    // RNGooglePlaces.getAutocompletePredictions(text)
    //   .then((results) => this.setState({renderPredictions: results}))
    //   .catch((error) => console.warn("error in:"+error));
  }

  async getLocationById(pre) {

    let hist = {
      id: pre.place_id,
      text: pre.description
    };

    let res = await AsyncStorage.getItem(STORAGE_KEY.SEARCH_HISTORY)
    if (res === null) {
      res = "[]"
    }

    res = JSON.parse(res);

    for (let i = 0; i < res.length; i++) {
      if (res[i].id === hist.id) {
        res.splice(i, 1);
        i--;
        break;
      }
    }

    res.push(hist);
    await AsyncStorage.setItem(STORAGE_KEY.SEARCH_HISTORY, JSON.stringify(res));
    let id = pre.place_id;
   await axios.get(`https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=${pre.description}&inputtype=textquery&fields=geometry&key=AIzaSyCfp9O4mgQSez58p_j5Dak_jEup3kSvHnc`)
      .then(result => result.data).then(result => {
      this.setState({lonlat: result.candidates})
    })
      .catch(err => console.warn(err));

    {this.state.lonlat.length>0&&this.state.lonlat.map(object=>
      {
        /*console.warn("candidates:"+JSON.stringify(this.state.lonlat));
        console.warn("geo"+JSON.stringify(this.state.lonlat[0].geometry.viewport.southwest.lat));*/
        let location = {
          latitude: object.geometry.location.lat,
          longitude:object.geometry.location.lng ,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }
        let query = {
          "query": {
            "bottom_left": {
              "lat": object.geometry.viewport.southwest.lat,
              "lon": object.geometry.viewport.southwest.lng
            },
            "top_right": {
              "lat": object.geometry.viewport.northeast.lat,
              "lon": object.geometry.viewport.northeast.lng
            }
          },
          "currency": "DOP",
        }
       axios.post(`${API}property/search`,query).then(res => res.data).then(res => Actions.Home({
          searchedResults: res.docs,
          location: location,
        })).catch(err => console.warn("In error"+err))
      }
    )
    }
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar
          translucent={false}
          backgroundColor={'#D2D2D2'}
          barStyle={'light-content'}
        />
        <View style={styles.headerContainer}>
          <CircleImage
            Image={require('../../assets/close.png')}
            onPress={() => Actions.Home()}
          />


          <InputField
            containerStyle={{
              borderRadius: RF(5),
              paddingLeft: RF(1),
              flex: 1,
              borderWidth: 0.5,
              elevation: 1,
              shadowOpacity: 1,
              shadowOffset: {width: 1, height: 1},
              backgroundColor: 'white',
              height: RF(7)
            }}
            placeholder={'Introduzca una ubicación'}
            autoFocus={true}
            onChangeText={(text) => {
              this.getPredictions(text)
            }
            }
          />
        </View>

        {
          this.state.renderPredictions.length > 0 && this.state.renderPredictions.map(prediction =>
            <ContentBar
              Text={prediction.description}
              prediction={prediction}
              touchable={false}
              onPress={this.getLocationById.bind(this)}
            />
          )}

        {/*history*/}
        <TouchableOpacity onPress={() => Actions.History()}>
          <View style={styles.historyContainer}>
            <Image style={styles.historyImage}
                   source={require('../../assets/history.png')}/>
            <ResponsiveText style={{color: '#7EAADA', fontSize: Fonts.h4}}> Historial</ResponsiveText>
          </View>
        </TouchableOpacity>

        <ContentBar
          Image={require('../../assets/location.png')}
          Text={'Llevame a donde estoy'}
          onPress={() => Actions.Home({currentLocation: 'CurrentLocation'})}
        />

      </SafeAreaView>
    );
  }
}

export default Search;
const styles = {
  headerContainer: {
    backgroundColor: '#D2D2D2',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: RF(2),
    borderBottomWidth: 0.8,
  },
  historyContainer: {
    borderBottomWidth: 0.5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  historyImage: {
    tintColor: '#7EAADA',
    width: RF(4),
    height: RF(4),
    margin: RF(3)
  }
}
