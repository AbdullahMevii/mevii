import React from 'react';
import {View, StatusBar, SafeAreaView, AsyncStorage} from 'react-native';
import Header from "../components/Header";
import RF from "react-native-responsive-fontsize";
import Fonts from "../config/styles/Fonts";
import Button from "../components/Button";
import LogoName from "../components/LogoName";
import {Actions} from "react-native-router-flux";
import {GoogleSignIn} from "../config/services/GoogleSignIn";
import {FacebookSignIn} from "../config/services/FacebookSignIn";


class Registrar extends React.Component {


  onGoogleSignInPress() {
    GoogleSignIn(
      (success) => {
        success = {...success, loggedInWith: "google"};
        AsyncStorage.setItem("USER", JSON.stringify(success)).then(res => console.warn("SET googleuser on register")).catch(error => console.warn("Error on googlelogin setitem"));
        Actions.Home(success);
      },
      (error) => {
        alert("Google sign out error:" + error)
      }
    )
  }

  onFacebookSignIn() {
    FacebookSignIn((success) => {
        success = {...success, loggedInWith: "fb"};
        AsyncStorage.setItem("USER", JSON.stringify(success)).then(res => console.warn("SET fbuser on register")).catch(error => console.warn("Error on fblogin setitem"));
        Actions.Home();
      },
      (error) => {
        alert("Facebook sign in error:" + error);
      })
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        <StatusBar backgroundColor="white" barStyle="dark-content"/>
        <Header
          onPress={() => Actions.pop()}
        />
        <LogoName
          containerStyle={styles.logo}
          text={'Registrar'}
          textStyle={styles.text}
        />

        <View style={{flex: 3}}>
          <Button
            btnText={styles.googleText}
            contentStyle={styles.google}
            containerStyle={{marginBottom: 10}}
            // containerStyle={styles.google}
            text={"Usando Google"}
            leftIcon={require('../../assets/google.png')}
            value={"Google"}
            onPress={this.onGoogleSignInPress.bind(this)}
          />
          <Button
            btnText={styles.facebookText}
            contentStyle={styles.facebook}
            containerStyle={{marginBottom: 10}}
            // containerStyle={styles.google}
            text={"Usando Facebook"}
            leftIcon={require('../../assets/facebook.png')}
            value={"Facebook"}
            onPress={this.onFacebookSignIn.bind(this)}
          />
          <Button
            btnText={styles.messageText}
            contentStyle={styles.message}
            containerStyle={{marginBottom: 10}}
            // containerStyle={styles.google}
            text={"Usando Correo electronico"}
            leftIcon={require('../../assets/message.png')}
            value={"Message"}
            onPress={() => Actions.UserRegister()}
          />

        </View>
      </SafeAreaView>
    );
  }
}

export default Registrar;
const styles = {
  text: {
    fontSize: Fonts.h1,
    color: '#317BC6',
    fontWeight: 'bold',
    // marginTop: RF(1.5)
  },
  google: {
    backgroundColor: '#DA4437',
    /*shadowOpacity: 4,*/
    elevation: RF(0.3),
    // marginBottom: RF(4),
    borderRadius: RF(1),
    paddingHorizontal: RF(3),
  },
  googleText: {
    color: 'white',
    textAlign: 'left',
    fontSize: Fonts.hMed,
  },
  facebook: {
    backgroundColor: '#3A5897',
    /*shadowOpacity: 4,*/
    elevation: RF(0.3),
    // marginBottom: RF(4),
    borderRadius: RF(1),
    paddingHorizontal: RF(3),
  },
  facebookText: {
    color: 'white',
    textAlign: 'left',
    fontSize: Fonts.hMed,
  },
  message: {
    backgroundColor: 'white',
    shadowOpacity: 4,
    elevation: RF(0.3),
    // marginBottom: RF(4),
    borderRadius: RF(1),
    paddingHorizontal: RF(3),
  },
  messageText: {
    color: 'grey',
    textAlign: 'left',
    fontSize: Fonts.hMed,
  },
  logo: {
    flex: 1,
    alignItems: 'center'

  }
}
