import React from 'react';
import {StatusBar, View, AsyncStorage, FlatList} from "react-native";
import RF from "react-native-responsive-fontsize";
import CircleImage from "../components/CircleImage";
import ResponsiveText from "../components/ResponsiveText";
import Fonts from "../config/styles/Fonts";
import ContentBar from "../components/ContentBar";
import {Actions} from "react-native-router-flux";
import {STORAGE_KEY} from "../config/Constants";

class History extends React.Component {
  state = {history: ''}
  len;

  async componentDidMount() {
    let res = await AsyncStorage.getItem(STORAGE_KEY.SEARCH_HISTORY).catch(error => console.warn(error));
    res = JSON.parse(res).reverse();
    res.length=4;
    this.setState({history: res})


  }

  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar
          backgroundColor={'#D2D2D2'}
          barStyle={'light-content'}
        />
        <View style={{
          backgroundColor: '#D2D2D2',
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: RF(2),
          borderBottomWidth: 0.8,
          height: RF(10),

        }}>
          <CircleImage
            containerStyle={{marginRight: RF(2)}}
            Image={require('../../assets/back.png')}
            ImageStyle={{tintColor: '#1F5EB6'}}
            onPress={() => Actions.pop()}

          />
          <ResponsiveText style={{fontSize: Fonts.h2, fontWeight: 'bold'}}>Historial</ResponsiveText>

        </View>
        {
          this.state.history.length > 0 && this.state.history.map(obj => <ContentBar
            Text={obj.text}
            onPress={()=>null}
            touchable={true}
          />)
        }


      </View>
    );
  }
}

export default History;
