const merc = new SphericalMercator();
import SphericalMercator from "@mapbox/sphericalmercator";

export const getZoomLevelFromRegion = (region, viewport) => {
  const { longitudeDelta } = region;
  const { width } = viewport;

  // Normalize longitudeDelta which can assume negative values near central meridian
  const lngD = (360 + longitudeDelta) % 360;

  // Calculate the number of tiles currently visible in the viewport
  const tiles = width / 256;

  // Calculate the currently visible portion of the globe
  const portion = lngD / 360;

  // Calculate the portion of the globe taken up by each tile
  const tilePortion = portion / tiles;

  // Return the zoom level which splits the globe into that number of tiles
  return Math.log2(1 / tilePortion);
};

export const getRegionByZoomLevel = (center, zoomLevel, viewport) => {
  const { latitude, longitude } = center;
  const { width, height } = viewport;

  // Calculate the projected coordinates center coordinates
  const [x, y] = merc.px([lng, lat], zoomLevel);

  // Subtract screen dimensions to get projected boundaries
  const xmin = Math.floor(x - width / 2);
  const xmax = xmin + width;
  const ymin = Math.floor(y - height / 2);
  const ymax = ymin + height;

  // Use reverse projection to convert boundaries to geographical coordinates
  const nw = merc.ll([xmin, ymin], zoomLevel);
  const se = merc.ll([xmax, ymax], zoomLevel);

  // Calculate latitude and longitude deltas as difference between boundary coordinates
  const latitudeDelta = nw[1] - se[1];
  const longitudeDelta = se[0] - nw[0];

  // Return react-native-maps compatible region
  return {
    latitude,
    longitude,
    latitudeDelta,
    longitudeDelta
  };
};

export const getRegionBoundaries = (center, zoomLevel, viewport) => {
  const { latitude, longitude, latitudeDelta, longitudeDelta } = center;
  const { width, height } = viewport;

  const [x, y] = merc.px([longitude, latitude], zoomLevel);
  const xmin = Math.floor(x - width / 2);
  const xmax = xmin + width;
  const ymin = Math.floor(y - height / 2);
  const ymax = ymin + height;

  const [westLongitude, northLatitude] = merc.ll([xmin, ymin], zoomLevel);
  const [eastLongitude, southLatitude] = merc.ll([xmax, ymax], zoomLevel);

  return {
    northLatitude,
    southLatitude,
    westLongitude,
    eastLongitude
  };
};
