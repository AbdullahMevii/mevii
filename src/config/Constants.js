export const API = "https://api.mevii.com/";
export const STORAGE_KEY = {
  USER: "USER",
  GOOGLE_USER: "GOOGLE_USER",
  SEARCH_HISTORY: "SEARCH_HISTORY"
};
