const Fonts = {
  h1: "6%",
  h2: "5%",
  h3: "4%",
  hMed: "4.5%",
  h4: "3.5%",
  h5: '3%'

}
export default Fonts;