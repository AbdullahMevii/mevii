const Color = {
    Primary: '#1E5EB6',
    PrimaryDark: '#28d38f',
    // PrimaryDark: '#B00638',
    Background: '#F9F9F9',
    PrimaryText: 'white',
    SecondaryText: '#ACACAC',
    Placeholder: "#ACACAC",
    BorderColor: 'rgba(40,231,164,0.3)',

};

export default Color;