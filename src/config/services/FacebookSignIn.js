import {LoginManager, AccessToken} from "react-native-fbsdk";

const FacebookSignIn = (success, error) => {
  LoginManager.logInWithReadPermissions(['public_profile']).then(
    function (result) {
      if (result.isCancelled) {
        alert("Login cancelled");
      } else {
        console.warn(
          "Login success with permissions: " +
          result.grantedPermissions.toString()
        );
        success();

      }
    },
    function (error) {
      alert(error);
    }
  );
}
export {FacebookSignIn};