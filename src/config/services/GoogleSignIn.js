import {GoogleSignin,statusCodes } from "react-native-google-signin";
import React from 'react';

const GoogleSignIn = async (onSuccess, onError) => {

    try {
        const isSignedIn = await GoogleSignin.isSignedIn();
        if (isSignedIn) {
            onSuccess(isSignedIn)
            alert("User is already signed in");
        } else {
            console.warn('not signed in. signing in');
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            onSuccess(userInfo)
        }
    } catch (error) {
        if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            alert("Sign in cancelled");
        } else if (error.code === statusCodes.IN_PROGRESS) {
            alert("in progress");
        } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            alert("play services not available");
        } else {
            console.warn(error);
            onError(error)
        }

        console.log(error)
    }
    ;


}
export {GoogleSignIn};
