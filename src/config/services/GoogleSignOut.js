import {GoogleSignin,statusCodes } from "react-native-google-signin";
const GoogleSignOut = async (success,error) => {
    try {
        await GoogleSignin.revokeAccess();
        let response=await GoogleSignin.signOut();
        success(response);
        /*this.setState({ user: null }); // Remember to remove the user from your app's state as well*/

    } catch (error) {
        error(error);
    }
};
export {GoogleSignOut};