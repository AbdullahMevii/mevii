import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, SafeAreaView} from 'react-native';
import RouterComponent from "./router/Router";
import {GoogleSignin} from "react-native-google-signin";

GoogleSignin.configure();

export default class App extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <RouterComponent/>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
console.disableYellowBox = false;
