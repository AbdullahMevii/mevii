import React from 'react';
import {createStackNavigator} from 'react-navigation';
import Welcome from "../src/screens/Welcome";
import Registrar from "../src/screens/Registrar";
import UserRegister from "../src/screens/UserRegister";
import LogIn from "../src/screens/LogIn";
import Home from "../src/screens/Home";

const AuthNavigator = createStackNavigator(
    {
        Welcome:Welcome,
        Registrar:Registrar,
        UserRegister:UserRegister,
        LogIn:LogIn,
        Home:Home
    },
    {
        initialRouteName: 'UserRegister',
        headerMode: 'none'
    }
);

export default AuthNavigator;