import React from 'react';
import {Scene, Router, Stack, Actions, ActionConst} from 'react-native-router-flux';
import Splash from "../src/screens/Splash";
import Welcome from "../src/screens/Welcome";
import UserRegister from "../src/screens/UserRegister";
import LogIn from "../src/screens/LogIn";
import Registrar from "../src/screens/Registrar";
import Home from "../src/screens/Home";
import Search from "../src/screens/Search";
import History from "../src/screens/History";
import CardDetail from "../src/screens/CardDetail";
import {BackHandler} from "react-native";
import Test from "../src/screens/Test";

const onBackPress = () => {
  if (Actions.state.index === 0) {
    return false
  }
  Actions.pop();
  return true
};

const RouterComponent = () => {
  return (
    <Router backAndroidHandler={onBackPress}>

      <Stack key="root" hideNavBar={true}>

        <Scene key="Splash"
               component={Splash}
               path={"Splash"}/>

        <Scene key="Welcome"
               component={Welcome}
               path={"Welcome"}
               type={ActionConst.RESET}/>

        <Scene key="LogIn"
               component={LogIn}
               path={"LogIn"}/>

        <Scene key="Registrar"
               component={Registrar}
               path={"Register"}/>

        <Scene key="UserRegister"
               component={UserRegister}
               path={"UserRegister"}/>

        <Scene key="Home"
               component={Home}
               path={"Home"}
               type={ActionConst.RESET}
               initial/>

        <Scene key="Search"
               component={Search}
               path={"Search"}/>

        <Scene key="History"
               component={History}
               path={"History"}/>

        <Scene key="CardDetail"
               component={CardDetail}
               path={"CardDetail"}/>

        {/*<Scene key="Test"*/}
        {/*component={Test}*/}
        {/*path={"Test"}*/}
        {/*initial/>*/}

      </Stack>
    </Router>


  );
}
export default RouterComponent;
