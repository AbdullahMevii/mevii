import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import Splash from "../src/screens/Splash";
import AuthNavigator from "./AuthNavigator";

const AppNavigator = createSwitchNavigator(
    {
        Splash: Splash,
        Auth: AuthNavigator
    },
    {
        initialRouteName: 'Auth',
        headerMode: 'none'
    }
);

export default AppNavigator;